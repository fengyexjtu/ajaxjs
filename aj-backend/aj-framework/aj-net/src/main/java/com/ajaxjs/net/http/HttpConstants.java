package com.ajaxjs.net.http;

public interface HttpConstants {
    String GET = "GET";
    String POST = "POST";

    String PUT = "PUT";

    String CONTENT_TYPE = "Content-Type";
}
