package com.ajaxjs.net.http;

/**
 * 常量
 * 
 * @author Frank Cheung
 *
 */
public interface HttpEnum {
	public final static String GET = "GET";
	public final static String POST = "POST";
	public final static String PUT = "PUT";
	public final static String DELETE = "DELETE";
}
