package com.ajaxjs.iam.resource_server;

import lombok.Data;

@Data
public class User {
	private Long id;

	private String name;
}
