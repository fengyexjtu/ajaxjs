package com.ajaxjs.developertools;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.ajaxjs.developertools")
public class BaseTestConfig {
}
