package com.ajaxjs.developertools.monitor.jvm.model;

import lombok.Data;

/**
 * Vm
 */
@Data
public class Vm {
    private int pid;

    private String name;
}
