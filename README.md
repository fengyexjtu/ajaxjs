
# AJ-Framework 基础支撑平台

[![License](https://img.shields.io/badge/license-Apache--2.0-green.svg?longCache=true&style=flat)](http://www.apache.org/licenses/LICENSE-2.0.txt)
[![Email](https://img.shields.io/badge/Contact--me-Email-orange.svg)](mailto:sp42@qq.com)
[![QQ群](https://framework.ajaxjs.com/static/qq.svg)](https://shang.qq.com/wpa/qunwpa?idkey=3877893a4ed3a5f0be01e809e7ac120e346102bd550deb6692239bb42de38e22) 


提供全栈的、简单的、轻量级的基础支撑平台，开源、免费。

- 前端：TypeScript + Vue.js 2 + iView UI
- 后端：Java 1.8 + Tomcat + MySQL + Spring 

[文档 Wiki](../../wikis) | [在线演示](https://cp-service.ajaxjs.com/demo/)

### 各项目一览

这是用一个源码空间（支持 Git/Svn）存放着多个项目，包含以下各个项目。

|项目名称|简介|文档|打包|
|------|-----|----|------|
|aj-parent  |Maven 根 POM|[README](aj-parent)|maven|
|aj-util|轻便灵巧的工具，有丰富的工具类和常用组件|[README](aj-util)|jar|
|aj-framework|常见的业务组件封装|[README](aj-framework)|jar|
|aj-web|网站作为独立客户端|[README](aj-web)|jar|
|aj-developer-tools |开发者工具|[README](aj-developer-tools)|jar|
|aj-sso|单点登录 SSO 中心|[README](aj-sso)|war|
|aj-sso-client |单点登录客户端|[README](aj-sso-client)|jar|
|aj-entity  |通用业务实体服务|[README](aj-entity)|war|
|aj-upload  |统一文件上传服务|[README](aj-upload)|war|
|aj-message  |统一邮件、短信发送服务|[README](aj-message)|war|
|aj-workflow  |工作流|[README](aj-upload)|war|
|aj-wechat  |微信公众号、支付服务|[README](aj-wechat)|war|
|aj-playground  |实验室|[README](aj-playground)|war|
|aj-ui|前端组件|[README](aj-ui)|npm|


# 联系 Contact
  [官网](https://framework.ajaxjs.com/) | 
  [源码](https://gitee.com/sp42_admin/ajaxjs) |
  [博客](https://zhangxin.blog.csdn.net/) | 
  [邮箱](mailto://sp42@qq.com) | 
  [QQ 群 3150067](//shang.qq.com/wpa/qunwpa?idkey=99415d164e2c776567c9370cc5b0bde26f4e2e7c5068978a24d1fe7c976ace93)

# 版权声明 License
作者版权所有，开源许可：Apache License, Version 2.0